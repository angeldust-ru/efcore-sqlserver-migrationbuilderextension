﻿using System;
using EFCore.SqlServer.MigrationBuilderExtensions.Common;
using Xunit;

namespace EFCore.SqlServer.MigrationBuilderExtensions.Tests
{
    public class DescriptionBuilderTests
    {
        private const string _author = "Autor name";
        private const string _summary = "Any summary";
        
        [Fact]
        public void AuthorSet_whenString_AuthorEqualsStringValue()
        {
        var description=new Description();
        var descriptionBuilder= new DescriptionBuilder(description);
        descriptionBuilder.Author(_author);
        Assert.Equal(_author,description.Author);
        }
        
        
        [Fact]
        public void AuthorSet_whenNull_ThrowsArgumentNullException()
        {
        var description=new Description();
        var descriptionBuilder= new DescriptionBuilder(description);
        Action act= () => descriptionBuilder.Author(null);
        var exception = Assert.Throws<ArgumentNullException>(act);
        Assert.Contains(Constants.DesctiptionBuilder.EXEPTION_SET_NULL_AUTHOR, exception.Message);
        }
       
        
        [Fact]
        public void SummarySet_whenString_SummaryEqualsStringValue()
        {
        var description=new Description();
        var descriptionBuilder= new DescriptionBuilder(description);
        descriptionBuilder.Summary(_summary);
        Assert.Equal(_summary,description.Summary);
        }
        
        
        [Fact]
        public void SummarySet_whenNull_ThrowsArgumentNullException()
        {
        var description=new Description();
        var descriptionBuilder= new DescriptionBuilder(description);
        Action act= () => descriptionBuilder.Summary(null);
        var exception = Assert.Throws<ArgumentNullException>(act);
        Assert.Contains(Constants.DesctiptionBuilder.EXEPTION_SET_NULL_SUMMARY, exception.Message);
        }
        
    }
}