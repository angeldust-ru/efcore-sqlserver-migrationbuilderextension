using System;
using EFCore.SqlServer.MigrationBuilderExtensions.Views;
using Xunit;

namespace EFCore.SqlServer.MigrationBuilderExtensions.Tests
{
    public class ViewBuilderTests
    {
        private readonly string _expected_Rody_Query_Sql=
            $"CREATE VIEW [].[]{Environment.NewLine}" +
            $"AS{Environment.NewLine}" +
            $"SELECT [t1] FROM [Test]{Environment.NewLine}";
            
        [Fact]
        public void Body_Query_Sql()
        {

            ViewElement ve=new ViewElement();
            ViewBuilder viewBuilder=new ViewBuilder(ve);
            viewBuilder.Body(query => 
                query
                    .From("Test")
                    .Select(new[] {"t1"}) 
            );
            string sql = ve.ToSQL();
            Assert.Equal(_expected_Rody_Query_Sql,sql);
        }
    }
}