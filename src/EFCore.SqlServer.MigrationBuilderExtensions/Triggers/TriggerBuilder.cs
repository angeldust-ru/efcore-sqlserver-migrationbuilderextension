using System.Collections.Generic;
using EFCore.SqlServer.MigrationBuilderExtensions.Common;

namespace EFCore.SqlServer.MigrationBuilderExtensions.Triggers
{
    public class TriggerBuilder:ElementBuilder<TriggerElement,TriggerBuilder>
    {
        
        public TriggerBuilder(TriggerElement trigger) : base(trigger)
        {
        }
        
        public TriggerBuilder TargetScheme(string scheme)
        {
            _target.TargetScheme = scheme;
            return this;
        }
        
        public TriggerBuilder TargetName(string name)
        {
            _target.TargetName= name;
            return this;
        }

        public TriggerBuilder AddAfterAction(TriggerAction action)
        {
            if(_target.AfterActions==null)_target.AfterActions=new List<TriggerAction>();
            _target.AfterActions.Add(action);
            return this;
        }
        
        
    }
}