using System;
using System.Globalization;

namespace EFCore.SqlServer.MigrationBuilderExtensions.Common
{
    public class DescriptionBuilder
    {
        private Description _description;
        public DescriptionBuilder(Description description)
        {
            _description = description;
        }

        public DescriptionBuilder Author(string author)
        {
            if(author==null) throw new ArgumentNullException(nameof(author),Constants.DesctiptionBuilder.EXEPTION_SET_NULL_AUTHOR);
            _description.Author = author;
            return this;
        }
        public DescriptionBuilder CreationDate(DateTime date)
        {
            _description.CreateDate = date;
            return this;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="date">Format: "dd.MM.yyyy hh:mm:ss" or "dd.MM.yyyy hh:mm" or "dd.MM.yyyy".  </param>
        /// <example>builder.CreationDate("22.10.2018")</example>
        /// <returns></returns>
        public DescriptionBuilder CreationDate(string date)
        {
            _description.CreateDate = DateTime.Parse(date, CultureInfo.InvariantCulture);
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="date">Format: "dd.MM.yyyy hh:mm:ss" or "dd.MM.yyyy hh:mm" or "dd.MM.yyyy".  </param>
        /// <param name="cultureInfo"></param>
        /// <example>builder.CreationDate("22.10.2018")</example>
        /// <returns></returns>
        public DescriptionBuilder CreationDate(string date,CultureInfo cultureInfo)
        {
            _description.CreateDate = DateTime.Parse(date, cultureInfo);
            return this;
        }
        public DescriptionBuilder Summary(string summary)
        {
            if(summary==null) throw new ArgumentNullException(nameof(summary),Constants.DesctiptionBuilder.EXEPTION_SET_NULL_SUMMARY);
            _description.Summary = summary;
            return this;
        }
        public DescriptionBuilder AddChangeLog(Action<DescriptionBuilder> logBuilder)
        {
            Description log=new Description();
            logBuilder(new DescriptionBuilder(log));
            _description.Log.Add(log);
            return this;
        }
    }
}