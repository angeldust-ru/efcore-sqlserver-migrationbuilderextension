namespace EFCore.SqlServer.MigrationBuilderExtensions.Common
{
    internal static class Constants
    {
        internal static class DesctiptionBuilder{
            internal static readonly string EXEPTION_SET_NULL_AUTHOR = "Author can not be set null value.";
            internal static readonly string EXEPTION_SET_NULL_SUMMARY = "Summary can not be set null value.";
            internal static readonly string EXEPTION_INVALID_DATE_TIME_FORMAT = "Invalid DateTime format.";
            
        }
    }
}